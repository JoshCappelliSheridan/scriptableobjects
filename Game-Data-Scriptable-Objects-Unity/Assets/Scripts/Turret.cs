﻿using UnityEngine;

public class Turret : MonoBehaviour {

    public float attackRange;
    public float attackInterval;
    public float attackPower;

    public GameObject bulletPrefab;
    public float bulletSpeed = 15;
    public float bulletLifetime = 2;

    public GameObject tank;
    public GameObject barrel;

    private float angleToPlayer;

    [HideInInspector]
    public float previousFiringTime;

    void Start() {
        GetTank();
    }

    void GetTank() {
        tank = GameObject.Find("Tank");
    }


    private void OnDrawGizmos() {
        UnityEditor.Handles.color = Color.yellow;
        UnityEditor.Handles.DrawWireDisc(transform.position, Vector3.back, attackRange);
    }

    void Update() {
        if (Vector3.Distance(transform.position, tank.transform.position) < attackRange) {
            RotateBarrel();
            previousFiringTime += Time.deltaTime;

            if (previousFiringTime > attackInterval) {
                Fire();
            }
        }
    }

    void RotateBarrel() {
        Vector3 directionToPlayer = tank.transform.position - transform.position;
        angleToPlayer = Mathf.Atan2(directionToPlayer.y, directionToPlayer.x) * Mathf.Rad2Deg;
        barrel.transform.eulerAngles = new Vector3(0, 0, angleToPlayer);
    }

    void Fire() {
        Bullet bullet = Instantiate(bulletPrefab, transform.position, barrel.transform.localRotation).GetComponent<Bullet>();
        bullet.Initialize(bulletSpeed, bulletLifetime);
        bullet.transform.parent = transform;
        previousFiringTime = 0;


    }
}
