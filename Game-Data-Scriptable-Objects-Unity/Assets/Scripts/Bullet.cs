﻿using UnityEngine;

public class Bullet : MonoBehaviour {

    private float speed;
    private float lifetime;
    private float aliveTime;

    Vector3 forward;

    public void Initialize(float speed, float lifetime) {
        this.speed = speed;
        this.lifetime = lifetime;
    }

    void Start() {
        forward = new Vector3(Mathf.Cos(transform.eulerAngles.z * Mathf.Deg2Rad), Mathf.Sin(transform.eulerAngles.z * Mathf.Deg2Rad), 0);
    }

    void Update() {
        aliveTime += Time.deltaTime;
        if (aliveTime > lifetime) {
            Destroy(gameObject);
            return;
        }
        Debug.DrawRay(transform.position, forward, Color.blue);
        transform.Translate(forward * speed * Time.deltaTime, Space.World);
    }
}
